# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_12_115718) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "channel"
    t.string "username"
    t.string "email"
    t.string "password"
    t.string "apitoken"
    t.string "apisecret"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sogou_plans"
    t.text "group_id_array"
    t.index ["user_id"], name: "index_accounts_on_user_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "adgroups", force: :cascade do |t|
    t.string "name"
    t.bigint "channel_id"
    t.bigint "campaign_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["campaign_id"], name: "index_adgroups_on_campaign_id"
  end

  create_table "campaigns", force: :cascade do |t|
    t.string "name"
    t.string "channel_id"
    t.bigint "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_campaigns_on_account_id"
  end

  create_table "cpcs", force: :cascade do |t|
    t.string "cpc_id"
    t.string "cpc_grp_id"
    t.string "cpc"
    t.string "price"
    t.string "visit_url"
    t.string "match_type"
    t.string "cpc_quality"
    t.boolean "pause"
    t.string "status"
    t.string "is_show"
    t.text "opt"
    t.text "opt_double"
    t.bigint "sogou_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "cpc_types"
    t.index ["sogou_group_id"], name: "index_cpcs_on_sogou_group_id"
  end

  create_table "keywords", force: :cascade do |t|
    t.string "channel_id"
    t.string "title"
    t.integer "adgroup_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sogou_groups", force: :cascade do |t|
    t.string "cpc_grp_id"
    t.string "cpc_plan_id"
    t.string "cpc_grp_name"
    t.float "max_price"
    t.text "negative_words"
    t.text "exact_negative_words"
    t.boolean "pause"
    t.integer "status"
    t.text "opt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "sogou_plans_id"
    t.string "sogou_plan_id"
    t.index ["sogou_plans_id"], name: "index_sogou_groups_on_sogou_plans_id"
  end

  create_table "sogou_ideas", force: :cascade do |t|
    t.string "cpc_idea_id"
    t.string "cpc_grp_id"
    t.string "title"
    t.string "description1"
    t.string "description2"
    t.string "mobile_visit_url"
    t.string "show_url"
    t.string "pause"
    t.string "status"
    t.text "opt"
    t.bigint "sogou_group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "visit_url"
    t.string "mobile_show_url"
    t.index ["sogou_group_id"], name: "index_sogou_ideas_on_sogou_group_id"
  end

  create_table "sogou_plans", force: :cascade do |t|
    t.string "plan_id"
    t.string "name"
    t.bigint "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cpc_plan_id"
    t.string "cpc_plan_name"
    t.string "budget"
    t.text "schedule"
    t.boolean "pause"
    t.boolean "join_union"
    t.string "mobile_price_rate"
    t.string "status"
    t.text "negative_words"
    t.bigint "accounts_id"
    t.text "regions"
    t.text "exact_negative_words"
    t.text "budget_offline_time"
    t.index ["account_id"], name: "index_sogou_plans_on_account_id"
    t.index ["accounts_id"], name: "index_sogou_plans_on_accounts_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "superadmin_role", default: false
    t.boolean "user_role", default: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "accounts", "users"
  add_foreign_key "adgroups", "campaigns"
  add_foreign_key "campaigns", "accounts"
  add_foreign_key "cpcs", "sogou_groups"
  add_foreign_key "sogou_groups", "sogou_plans", column: "sogou_plans_id"
  add_foreign_key "sogou_ideas", "sogou_groups"
  add_foreign_key "sogou_plans", "accounts"
  add_foreign_key "sogou_plans", "accounts", column: "accounts_id"
end
