class AddSogouPlansToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :sogou_plans, :string
  end
end
