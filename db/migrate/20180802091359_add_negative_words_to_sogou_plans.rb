class AddNegativeWordsToSogouPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_plans, :negative_words, :text
  end
end
