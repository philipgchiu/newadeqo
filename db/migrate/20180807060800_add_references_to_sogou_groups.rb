class AddReferencesToSogouGroups < ActiveRecord::Migration[5.2]
  def change
    add_reference :sogou_groups, :sogou_plans, foreign_key: true
  end
end
