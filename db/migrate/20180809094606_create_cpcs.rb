class CreateCpcs < ActiveRecord::Migration[5.2]
  def change
    create_table :cpcs do |t|
      t.string :cpc_id
      t.string :cpc_grp_id
      t.string :cpc
      t.string :price
      t.string :visit_url
      t.string :match_type
      t.string :cpc_quality
      t.boolean :pause
      t.string :status
      t.string :is_show
      t.text :opt
      t.text :opt_double
      t.references :sogou_group, foreign_key: true
      t.timestamps
    end
  end
end
