class AddBudgetOfflineTimeToSogouPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_plans, :budget_offline_time, :text
  end
end
