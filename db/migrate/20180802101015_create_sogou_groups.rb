class CreateSogouGroups < ActiveRecord::Migration[5.2]
  def change
    create_table :sogou_groups do |t|
      t.string :cpc_grp_id
      t.string :cpc_plan_id
      t.string :cpc_grp_name
      t.float :max_price
      t.text :negative_words
      t.text :exact_negative_words
      t.boolean :pause
      t.integer :status
      t.text :opt
      t.timestamps
    end
  end
end
