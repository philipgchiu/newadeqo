class AddRegionsToSogouPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_plans, :regions, :text
  end
end
