class ChangeSogouIdeaName < ActiveRecord::Migration[5.2]
  def change
  	rename_column :sogou_ideas, :visit_url, :mobile_visit_url
  end
end
