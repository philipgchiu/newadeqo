class AddMobileShowUrlToSogouIdeas < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_ideas, :mobile_show_url, :string
  end
end
