class AddSogouPlanIdToSogouGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_groups, :sogou_plan_id, :string
  end
end
