class CreateSogouPlans < ActiveRecord::Migration[5.2]
  def change
    create_table :sogou_plans do |t|
      t.string :plan_id
      t.string :name
      t.references :account, foreign_key: true

      # to add: schedule, budget

      t.timestamps
    end
  end
end
