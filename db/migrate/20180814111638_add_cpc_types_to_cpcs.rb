class AddCpcTypesToCpcs < ActiveRecord::Migration[5.2]
  def change
    add_column :cpcs, :cpc_types, :text
  end
end
