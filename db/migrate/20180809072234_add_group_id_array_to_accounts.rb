class AddGroupIdArrayToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :group_id_array, :text
  end
end
