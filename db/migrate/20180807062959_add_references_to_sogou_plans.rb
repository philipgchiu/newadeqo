class AddReferencesToSogouPlans < ActiveRecord::Migration[5.2]
  def change
    add_reference :sogou_plans, :accounts, foreign_key: true
  end
end
