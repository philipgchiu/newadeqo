class CreateKeywords < ActiveRecord::Migration[5.2]
  def change
    create_table :keywords do |t|
      t.string :channel_id
      t.string :title
      t.integer :adgroup_id

      t.timestamps
    end
  end
end
