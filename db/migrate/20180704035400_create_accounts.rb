class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :channel
      t.string :username
      t.string :email
      t.string :password
      t.string :apitoken
      t.string :apisecret
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
