class AddColumnsToSogouPlans < ActiveRecord::Migration[5.2]
  def change
    add_column :sogou_plans, :cpc_plan_id, :string
    add_column :sogou_plans, :cpc_plan_name, :string
    add_column :sogou_plans, :budget, :string
    add_column :sogou_plans, :schedule, :text
    add_column :sogou_plans, :pause, :boolean
    add_column :sogou_plans, :join_union, :boolean
    add_column :sogou_plans, :mobile_price_rate, :string
    add_column :sogou_plans, :status, :string
  end
end
