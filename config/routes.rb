Rails.application.routes.draw do
  get 'cpc/_cpc'
  get 'cpc/index'
  get 'cpc/edit'
  get 'cpc/show'
  get 'cpc/create'
  get 'sogou_ideas/edit'
  get 'sogou_ideas/index'
  get 'sogou_ideas/new'
  get 'sogou_ideas/show'
  get 'sogou_ideas/_sogou_idea'
  get 'sogou_groups/edit'
  get 'sogou_groups/index'
  get 'sogou_groups/new'
  get 'sogou_groups/show'
  get 'sogou_plans/edit'
  get 'sogou_plans/index'
  get 'sogou_plans/new'
  get 'sogou_plans/show'
  get 'sogou_plans/_form'
  get 'sogou_plans/new'
  get 'sogou_plans/show'
  get 'sogou_plans/index'
  get 'sogou_plans/edit'
  
  post '/send_demo_email' => 'home#send_demo_email'
  post '/send_contact_us_email' => 'home#send_contact_us_email'
  
  get '/dashboard' => 'home#dashboard'
  
  require 'sidekiq/web'

  authenticate :user, lambda { |u| u.superadmin_role? } do
  mount Sidekiq::Web => "/sidekiq"
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  end

  resources :campaigns
  resources :accounts
  resources :adgroups
  resources :keywords

  resources :sogou_plans
  resources :sogou_groups
  resources :sogou_ideas
  resources :cpcs
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  scope "/superadmin" do
    resources :users
  end

  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  # 360's actions 

  get 'accounts/:id/retrieve_campaigns' => 'accounts#retrieve_campaigns', as: :accounts_retrieve_campaigns
  get 'campaigns/:id/retrieve_adgroups' => 'campaigns#retrieve_adgroups', as: :campaigns_retrieve_adgroups
  get 'adgroups/:id/retrieve_keywords' => 'adgroups#retrieve_keywords', as: :adgroup_retrieve_keywords
  get 'accounts/:id/delete_campaigns' => 'accounts#delete_campaigns', as: :accounts_delete_campaigns
  get 'campaigns/:id/delete_adgroups' => 'campaigns#delete_adgroups', as: :campaigns_delete_adgroups
  get 'adgroups/:id/delete_keywords' => 'adgroups#delete_keywords', as: :adgroup_delete_keywords
  get 'superadmin/:id/make_superadmin' => 'users#make_superadmin', as: :make_superadmin

  # sogou's actions

  get 'accounts/:id/retrieve_plans' => 'accounts#sogou_retrieve_plans', as: :accounts_retrieve_plans
  get 'accounts/:id/delete_plans' => 'accounts#delete_plans', as: :accounts_delete_plans
  get 'sogou_plans/:id/retrieve_groups' => 'sogou_plans#sogou_retrieve_groups', as: :sogou_retrieve_groups
  get 'sogou_plans/:id/delete_groups' => 'sogou_plans#sogou_delete_groups', as: :sogou_delete_groups
  get 'sogou_groups/:id/retrieve_ideas' => 'sogou_groups#sogou_retrieve_ideas', as: :sogou_retrieve_ideas
  get 'sogou_groups/:id/delete_ideas' => 'sogou_groups#sogou_delete_ideas', as: :sogou_delete_ideas
  get 'sogou_groups/:id/retrieve_cpcs' => 'sogou_groups#sogou_retrieve_cpcs', as: :sogou_retrieve_cpcs
  get 'sogou_groups/:id/delete_cpcs' => 'sogou_groups#sogou_delete_cpcs', as: :sogou_delete_cpcs
end
