lock '3.10.1'

set :application,     'adeqo'
set :repo_url,        'git@bitbucket.org:philipgchiu/newadeqo.git'
set :user,            'surendradev'

# Don't change these unless you know what you're doing
set :pty,             false
set :use_sudo,        false
# set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:stage)}/#{fetch(:application)}"

set :puma_threads,    [4, 16]
set :puma_workers,    1
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-#{fetch(:stage)}.sock"
set :puma_state,      "#{shared_path}/tmp/pids/#{fetch(:stage)}.state"
set :puma_pid,        "#{shared_path}/tmp/pids/#{fetch(:stage)}.pid"
set :puma_access_log, "#{shared_path}/log/#{fetch(:stage)}.error.log"
set :puma_error_log,  "#{shared_path}/log/#{fetch(:stage)}.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord

set :ssh_options,     { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }

## Defaults:
set :branch,        "master"
set :format,        :pretty
# set :log_level,     :debug
set :keep_releases, 3

## Linked Files & Directories (Default None):
set :linked_files, %w{config/database.yml}
set :linked_dirs,  %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart
end

namespace :rails do
  desc 'Open a rails console `cap [staging] rails:console [server_index default: 0]`'
  task :console => 'rvm1:hook' do
    on roles(:app) do |server|
      server_index = ARGV[2].to_i
      return if server != roles(:app)[server_index]
      puts "Opening a console on: #{host}...."
      cmd = "ssh #{server.user}@#{host} -t 'cd #{fetch(:deploy_to)}/current && ( export RAILS_ENV='production' RAILS_GROUPS="" ; ~/.rvm/bin/rvm default do bundle exec rails console )'"
      puts cmd
      exec cmd
    end
  end
end

desc 'Open a ssh `cap [staging] ssh [server_index default: 0]`'
task :ssh do
  on roles(:app) do |server|
    server_index = ARGV[2].to_i
    return if server != roles(:app)[server_index]
    puts "Opening a console on: #{host}...."
    cmd = "ssh #{server.user}@#{host}"
    puts cmd
    exec cmd
  end
end
