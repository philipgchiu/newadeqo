class ContactUsMailer < ApplicationMailer
  default from: 'Adeqo <do-not-reply@adeqo.com>'

  def send_contact_us_email(subject, body)
    to = "sales@bmgww.com"
    cc = "surendra.we@gmail.com"

    mail(to: to, cc: cc, subject: subject, body: body, content_type: "text/html")
  end

end
