json.extract! campaign, :id, :account_id, :name, :budget, :impressions, :conversion, :account_id, :created_at, :updated_at
json.url campaign_url(campaign, format: :json)
