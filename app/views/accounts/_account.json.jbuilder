json.extract! account, :id, :channel, :username, :email, :password, :apitoken, :apisecret, :user_id, :created_at, :updated_at
json.url account_url(account, format: :json)
