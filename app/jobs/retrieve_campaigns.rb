class RetrieveCampaigns < ActiveJob::Base
  queue_as :default

  def perform(account_id)
    @account = Account.find(account_id)
    @username = @account.username
    @password = @account.password
    @apitoken = @account.apitoken
    @apisecret = @account.apisecret
    @target = "campaigns"

    message = ApiCall::DataRetriever.new(username: @username, password: @password, apitoken: @apitoken, apisecret: @apisecret).call_api(reference_element: @account, target: @target)
  end

end
