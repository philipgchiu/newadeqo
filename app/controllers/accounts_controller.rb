class AccountsController < ApplicationController

  layout 'site'

  before_action :find_account, only: [:show, :edit, :update, :destroy, :retrieve_campaigns]
  before_action :owned_account, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def retrieve_campaigns
    @username = @account.username
    @password = @account.password
    @apitoken = @account.apitoken
    @apisecret = @account.apisecret
    @data_retriever = ApiCall::DataRetriever.new(username: @username, password: @password, apitoken: @apitoken, apisecret: @apisecret)

    @response = @data_retriever.threesixty_login
    @accesstoken = @response["account_clientLogin_response"]["accessToken"]
    @retrieved_campaigns_ids = @data_retriever.threesixty_campaignids(@accesstoken)

    @account_campaigns = @account.campaigns
    RetrieveCampaigns.perform_later(@account.id)

    if @account.campaigns.count < 1
    @message = "Campaigns for #{@account.channel}-#{@account.username} are being retrieved. Check the account page shortly."
    else
      @account_campaign_ids = []
      @account.campaigns.each do |a| @account_campaign_ids.push(a.channel_id)
      end
      if
        @retrieved_campaigns_ids.sort == @account_campaign_ids.sort
        @message = "Campaigns for #{@account.channel}-#{@account.username} are already up to date."
      else
        @message = "Campaigns for #{@account.channel}-#{@account.username} are being updated. Check the campaign page shortly."
      end
    end
    redirect_to accounts_path
    flash[:success] = @message
    # flash[:success] = message
  end

  def sogou_retrieve_plans
    @account = Account.find(params[:id])
    @username = @account.email
    @password = @account.password
    @apitoken = @account.apitoken

    # @plan_ids = sogou_retrieve_plan_ids(@username,@password,@apitoken)

    @sogou_api = ApiCall::Sogou.new(@username, @password, @apitoken).sogou_api("CpcPlanService")

    @update_status = @sogou_api.call(:get_all_cpc_plan)

    @plans = []
    @plans = @update_status.body.to_hash[:get_all_cpc_plan_response][:cpc_plan_types]

    @plans.each do |p|
      begin
        sg = SogouPlan.create(p)

        @account.sogou_plans.push(sg)
      rescue
        p 'got some error'
        next
      end
    end
    @account.save
    redirect_to @account

    flash[:success] = "Plans for #{@account.channel}-#{@account.username} have been successfully retrieved."
  end


  def delete_campaigns
    # operates under the assumption that user has already created an account.
    @account = Account.find(params[:id])
    @current_campaigns = @account.campaigns

    if @account.campaigns.count > 0
      @account.campaigns.destroy_all
      flash[:success] = "Your campaigns have been deleted."
    else
      flash[:success] = "No campaigns to delete."
    end
    redirect_to @account

  end

  def delete_plans
    # operates under the assumption that user has already created an account.
    @account = Account.find(params[:id])

    if @account.sogou_plans.count > 0
      @account.sogou_plans.destroy_all
      flash[:success] = "Your plans have been deleted."
    else
      flash[:success] = "No plans to delete."
    end
    redirect_to @account

  end

  def index
    @user = current_user
    @accounts = @user.accounts
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = current_user.accounts.build
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = current_user.accounts.build(account_params)
    # @account = Account.new(account_params)
    @success = false

    @login = nil

    if @account.channel == "Sogou"
      # do stuff with sogou

      @sogou_api = ApiCall::Sogou.new(@account.email, @account.password, @account.apitoken).sogou_api('AccountService')

      @login = @sogou_api.call(:get_account_info)
      @header = @login.header

      @err = ""
      @messg = ""
      respond_to do |format|
        if @header[:res_header][:desc] == "success"
          # create the account and save to database
          @account.save
          format.html { redirect_to accounts_path, notice: @header[:res_header][:desc].to_s + ': Account was successfully created.' }
          format.json { render :show, status: :created, location: @account }
        else
          @account = Account.new(account_params)

          if @header[:res_header].key?(:failures)
            code = @header[:res_header][:failures][:code]
            if code == "6"
              @err = "Error Code 6: Invalid Username. Original message: "
            elsif code == "8"
              @err = "Error Code 8: Invalid Password. Original message: "
            elsif code == "10"
              @err = "Error Code 10: Invalid Token. Original message: "
            else
              @err = "Unknown Error took place. Original message: "
            end

            @err += @header[:res_header][:failures][:message]
          end

          # 6用户名无效
          # username failure


          # {:res_header=>{:desc=>"failure", :failures=>{:code=>"8", :message=>"密码无效"
          # password failure

          # :failures=>{:code=>"10", :message=>"token无效",


          flash[:notice]= @err
          format.html { render new_account_path }
          format.json { render json: @login.to_s, status: :unprocessable_entity }


        end
        # will include english translations of failures

      end

    else
      # do stuff with 360
      @data_retriever = ApiCall::DataRetriever.new(username: @account.username, password: @account.password, apitoken: @account.apitoken, apisecret: @account.apisecret)

      @login = @data_retriever.threesixty_login

      @err = ""
      @messg = ""
      respond_to do |format|
        if @login != "ERROR"
          if !@login["account_clientLogin_response"].key?("failures") && @login != "ERROR"
            @account.save
            format.html { redirect_to accounts_path, notice: @login.to_s + 'Account was successfully created.' }
            format.json { render :show, status: :created, location: @account }
          else

            @account = Account.new(account_params)

            if @login["account_clientLogin_response"].key?("failures")
              @err = @login["account_clientLogin_response"]["failures"]["item"]["message"].to_s
            else
              # @err = @login.to_s # this should be "ERROR"
            end

            if @err == "登录失败次数超限；每天登录失败次数限制5次；"
              @messg = "You have exceeded the number of failed login attempts (5). Try again in 24 hours. " + @err
            elsif @err == "登录失败；密码格式有误；"
              @messg = "The password for this account is incorrect. Make sure you are trying to access the correct account. " + @err
            elsif @err == "登录失败；用户名和密码不匹配；"
              @messg = "Unable to log in. Make sure you have entered the correct API tokens. "
            elsif @err == "登录失败；无法获取到用户ID；"
              @messg = "Couldn't find user. Make sure your credentials are correct. "            else
              @messg = "An unknown error occurred. " + @err
            end
            flash[:notice]= @messg
            format.html { render new_account_path }
            format.json { render json: @login.to_s, status: :unprocessable_entity }
          end
        else
          @account = Account.new(account_params)
          @messg = "Unexpected error. Please make sure your API tokens are correct."
          flash[:notice]= @messg
          format.html { render new_account_path }
          format.json { render json: @login.to_s, status: :unprocessable_entity }
      end
    end
  end



  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def find_account
      @account = Account.find(params[:id])
    end

    def owned_account
      unless current_user.id == @account.user_id
      redirect_to root_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:channel, :username, :email, :password, :apitoken, :apisecret, :user_id)
    end

end
