class SogouIdeasController < ApplicationController
  layout 'site'
  before_action :find_sogou_idea, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :owned_sogou_idea, only: [:show, :edit, :update, :destroy]
  def new
    @sogou_idea = SogouIdea.new
  end

  def create
    @sogou_idea = SogouIdea.new(sogou_idea_params)
  end

  def new
  end

  def show
  end

  def destroy
    session[:return_to] ||= request.referer
    @sogou_idea.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'Idea was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

   private 
    def find_sogou_idea
      @sogou_idea = SogouIdea.find(params[:id])
    end

    def owned_sogou_idea
      unless current_user.id == @sogou_idea.sogou_group.sogou_plan.account.user_id
      redirect_to root_path
      end
    end
     def sogou_group_params
      params.require(:sogou_group).permit(:cpc_idea_id, :cpc_grp_id, :title, 
        :description1,:description2,:mobile_visit_url,:visit_url,:mobile_show_url,:show_url, 
        :pause, :status,:opt)
    end


end
