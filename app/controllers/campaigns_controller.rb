class CampaignsController < ApplicationController
  layout 'site'
  before_action :find_campaign, only: [:show, :edit, :update, :destroy, :retrieve_adgroups, :delete_adgroups]
  before_action :authenticate_user!
  before_action :owned_campaign, only: [:show, :edit, :update, :destroy]

  def retrieve_adgroups
    @username = @campaign.account.username
    @password = @campaign.account.password
    @apitoken = @campaign.account.apitoken
    @apisecret = @campaign.account.apisecret
    @data_retriever = ApiCall::DataRetriever.new(username: @username, password: @password, apitoken: @apitoken, apisecret: @apisecret)

    @response = @data_retriever.threesixty_login
    @accesstoken = @response["account_clientLogin_response"]["accessToken"]
    @retrieved_adgroup_ids = @data_retriever.threesixty_adgroupid(@accesstoken, @campaign.channel_id)
    @campaign_adgroups = @campaign.adgroups

    CampaignWorker.perform_async(@campaign.id, 'retrieve_adgroups')

    if @campaign_adgroups.count < 1
    @message = "Adgroups for #{@campaign.name} are being retrieved. Check the campaign page shortly."
    else
      @campaign_adgroup_ids = []
      @campaign.adgroups.each do |a| @campaign_adgroup_ids.push(a.channel_id)
      end
      if @retrieved_adgroup_ids.sort == @campaign_adgroup_ids.sort
        @message = "Adgroups for #{@campaign.name} are already up to date."
      else
        @message = "Adgroups for #{@campaign.name} are being updated. Check the campaign page shortly."
      end
    end
      redirect_to @campaign.account
      flash[:success] = @message
  end

  def delete_adgroups
    # operates under the assumption that user has already created an account.
    if @campaign.adgroups.count > 0

      CampaignWorker.perform_async(@campaign.id, 'delete_all_adgroups')

      redirect_to @campaign.account
      flash[:success] = "Adgroups for #{@campaign.name} are being deleted. Check the campaign page shortly."
    else
      redirect_to @campaign
      flash[:success] = "No Adgroups to delete."
    end
  end

  def index
    @user = current_user
    @accounts = Account.where(:user_id => @user.id).all
    if @accounts.first
      @accounts.each do |account|
        @campaigns=Campaign.where(:account_id => account.id).all
      end
    else
      @campaigns=[]
    end
    @campaigns
  end

  def show
  end

  def new
    @campaign = Campaign.new
  end

  def edit
  end

  def create
    @campaign = Campaign.new(campaign_params)

    respond_to do |format|
      if @campaign.save
        format.html { redirect_to @campaign, notice: 'Campaign was successfully created.' }
        format.json { render :show, status: :created, location: @campaign }
      else
        format.html { render :new }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @campaign.update(campaign_params)
        format.html { redirect_to @campaign, notice: 'Campaign was successfully updated.' }
        format.json { render :show, status: :ok, location: @campaign }
      else
        format.html { render :edit }
        format.json { render json: @campaign.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    session[:return_to] ||= request.referer
    @campaign.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'Campaign was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def find_campaign
      @campaign = Campaign.find(params[:id])
    end

    def owned_campaign
      unless current_user.id == @campaign.account.user_id
      redirect_to root_path
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def campaign_params
      params.require(:campaign).permit(:account_id, :name, :budget, :impressions, :clicks)
    end
end
