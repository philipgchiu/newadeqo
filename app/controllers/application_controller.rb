class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  require 'rubygems'
  require 'htmlentities'
  require 'simple-rss'
  require 'open-uri'

  def emailcheck
    @user = User.search(params[:email])
  end

end
