class CpcController < ApplicationController
  before_action :find_cpc, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :owned_cpc, only: [:show, :edit, :update, :destroy]

  def new
    @sogou_cpc = Cpc.new
  end

  def create
    @sogou_cpc = Cpc.new(sogou_cpc_params)
  end

  def new
  end

  def show
  end

  def destroy
    session[:return_to] ||= request.referer
    @sogou_cpc.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'CPC was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

   private 
    def find_cpc
      @sogou_cpc = Cpc.find(params[:id])
    end

    def owned_cpc
      unless current_user.id == @sogou_cpc.sogou_group.sogou_plan.account.user_id
      redirect_to root_path
      end
    end
     def sogou_cpc_params
      params.require(:sogou_group).permit(:cpc_id, :cpc_grp_id, :cpc, :price, :visit_url, :match_type, :cpc_quality, :pause, :status, :is_show, :opt,'@xmlns:ns2')
    end
end
