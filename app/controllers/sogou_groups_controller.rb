class SogouGroupsController < ApplicationController
  layout 'site'
  before_action :find_sogou_group, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :owned_sogou_group, only: [:show, :edit, :update, :destroy]
  def new
    @sogou_group = SogouGroup.new
  end

  def create
    @sogou_group = SogouGroup.new(sogou_group_params)
  end

  def sogou_retrieve_cpcs

    @group = SogouGroup.find(params[:id])
    @account = @group.sogou_plan.account
    @username = @account.email
    @password = @account.password
    @apitoken = @account.apitoken

    @sogou_obj = ApiCall::Sogou.new(@username, @password, @apitoken)
    @group_id_array = [@group.cpc_grp_id.to_i]

    @user = @sogou_obj.sogou_api("CpcService")

    # retrieve response to API call, convert to hash

    @update_status = @user.call(:get_cpc_id_by_cpc_grp_id, message: {cpcGrpIds: @group_id_array, getTemp: 0})

    @stathash = @update_status.body.to_hash

    # retrieve the ideas from the hash

    @cpc = @stathash[:get_cpc_id_by_cpc_grp_id_response][:cpc_grp_cpc_ids]

    # @cpcs.each do |cpc|

    if @cpc.keys.count >= 2
      begin
        @status = @user.call(:get_cpc_by_cpc_id, message: { cpcIds: @cpc[:cpc_ids] })
        cpc_to_s = @status.body.to_hash

        cpc_to_s = cpc_to_s[:get_cpc_by_cpc_id_response]
        cpc_to_s.delete(:"@xmlns:ns2")
        cpc_to_s.delete(:"@xmlns:ns3")

      rescue
        puts 'Error'
      end

      unless cpc_to_s[:cpc_types].nil?
        for j in 0..cpc_to_s[:cpc_types].length - 1
          cpc_tos = cpc_to_s[:cpc_types][j]

          if !cpc_tos.nil? and cpc_tos.has_key?(:mobile_visit_url)
            cpc_tos.delete(:mobile_visit_url)
          end

          cpc_to_su = Cpc.new(cpc_tos)
          @group.cpcs.push(cpc_to_su)

        end
      end
    end
    # end

    @account.save
    redirect_to @group
    flash[:success] = "CPCs for #{@account.channel}-#{@account.username} retrieved."


  end

  def sogou_retrieve_ideas
  	@group = SogouGroup.find(params[:id])
    @account = @group.sogou_plan.account
    @username = @account.email
    @password = @account.password
    @apitoken = @account.apitoken

    @sogou_obj = ApiCall::Sogou.new(@username, @password, @apitoken)
    @group_id_array = [@group.cpc_grp_id.to_i]

    @user = @sogou_obj.sogou_api("CpcIdeaService")

    # retrieve response to API call, convert to hash

    @update_status = @user.call(:get_cpc_idea_by_cpc_grp_id, message: { cpcGrpIds: @group_id_array, getTemp: 0 })

    @stathash = @update_status.body.to_hash

    # retrieve the ideas from the hash

    @ideas = @stathash[:get_cpc_idea_by_cpc_grp_id_response][:cpc_grp_ideas]

    group_id = @ideas[:cpc_grp_id]
    if group_id == @group.cpc_grp_id
      unless @ideas[:cpc_idea_types].nil?
      	for i in 0..@ideas[:cpc_idea_types].length - 1
      		ideax = @ideas[:cpc_idea_types][i]
      		sgi = SogouIdea.create(ideax)
      		@group.sogou_ideas.push(sgi)
    		end
      end
	  end

    @account.save
    redirect_to @group
    flash[:success] = "Ideas for #{@account.channel}-#{@account.username} retrieved."

  end

  def sogou_delete_ideas
    # operates under the assumption that user has already created an account.
    @sogou_group = SogouGroup.find(params[:id])

    if @sogou_group.sogou_ideas.count > 0
      @sogou_group.sogou_ideas.destroy_all
      flash[:success] = "Your ideas have been deleted."
    else
      flash[:success] = "No ideas to delete."
    end
    redirect_to @sogou_group
  end

  def sogou_delete_cpcs
    # operates under the assumption that user has already created an account.
    @sogou_group = SogouGroup.find(params[:id])

    if @sogou_group.cpcs.count > 0
      @sogou_group.cpcs.destroy_all
      flash[:success] = "Your cpcs have been deleted."
    else
      flash[:success] = "No cpcs to delete."
    end
    redirect_to @sogou_group
  end
   def destroy
    session[:return_to] ||= request.referer
    @sogou_group.destroy
    respond_to do |format|
      format.html { redirect_to session.delete(:return_to), notice: 'Plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private
  	def find_sogou_group
      @sogou_group = SogouGroup.find(params[:id])
    end

    def owned_sogou_group
      unless current_user.id == @sogou_group.sogou_plan.account.user_id
      redirect_to root_path
      end
    end
     def sogou_group_params
      params.require(:sogou_group).permit(:cpc_grp_id, :cpc_plan_id, :cpc_grp_name,
      	:max_price,:negative_words, :exact_negative_words,:pause, :status, :opt)
    end

end