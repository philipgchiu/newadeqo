class CampaignWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(campaign_id, action)
    campaign = Campaign.find(campaign_id)

    if action == "retrieve_adgroups"
      username = campaign.account.username
      password = campaign.account.password
      apitoken = campaign.account.apitoken
      apisecret = campaign.account.apisecret

      @data_retriever = ApiCall::DataRetriever.new(username: username, password: password, apitoken: apitoken, apisecret: apisecret)
      @message = @data_retriever.call_api(reference_element: campaign, target: 'adgroups')

      puts "====>      #{@message}     <===="

    elsif action == "delete_all_adgroups"
      campaign.adgroups.destroy_all
      message = "Your Adgroups have been deleted. Refresh the page."
    end
  end
end
