module ApplicationHelper
  def alert_for(flash_type)
  { success: 'alert-success',
    error: 'alert-danger',
    alert: 'alert-warning',
    notice: 'alert-info'
  }[flash_type.to_sym] || flash_type.to_s
  end

    def user_campaigns
      @user = current_user
      @accounts = Account.where(:user_id => @user.id).all
      @campaigns = []
        if @accounts.first
          @accounts.each do |account|
            @user_campaigns = Campaign.where(:account_id => account.id).all
              if @user_campaigns.count >0
                @user_campaigns.each do |c|
                  @campaigns.push(c)
                end
            end
          end
        end
          if @campaigns.count >0
              @campaigns.each_with_index do |campaign, index|
            end
          else
            []
          end
      end

    def user_adgroups
      @campaigns = user_campaigns
      @adgroups = []
      if @campaigns.first
        @campaigns.each do |campaign|
          campaign.adgroups.each do |adgroup|
          @adgroups.push(adgroup)
          end
        end
      end
  
      if @adgroups.count >0
        @adgroups.each_with_index do |adgroup, index|
        end
      else
        []
      end
    end
    
    def user_keywords
      @adgroups = user_adgroups
      @keywords = []
      if @adgroups.first
        @adgroups.each do |adgroup|
          adgroup.keywords.each do |keyword|
          @keywords.push(keyword)
          end
        end
      end
  
      if @keywords.count >0
        @keywords.each_with_index do |keyword, index|
        end
      else
        []
      end
    end
end
