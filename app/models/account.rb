class Account < ApplicationRecord
  belongs_to :user
  has_many :campaigns, :dependent => :destroy
  has_many :sogou_plans, :dependent => :destroy
  serialize :group_id_array, Array
end
