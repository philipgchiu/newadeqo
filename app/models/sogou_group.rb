class SogouGroup < ApplicationRecord
  belongs_to :sogou_plan
      #   t.text :negative_words
      # t.text :exact_negative_words
  serialize :negative_words, Array
  serialize :exact_negative_words, Array
  has_many :sogou_ideas, dependent: :destroy
  has_many :cpcs, dependent: :destroy
end

