class Campaign < ApplicationRecord
  belongs_to :account
  has_many :adgroups, dependent: :destroy
end
