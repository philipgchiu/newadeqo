class SogouPlan < ApplicationRecord
	belongs_to :account
	serialize :schedule, Array
	has_many :sogou_groups, dependent: :destroy
end
