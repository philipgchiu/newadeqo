// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require bootstrap-sprockets
//= require home
//= require jquery.bxslider.min
//= require jquery.ellipsis
//= require jquery.fileDownload
//= require jquery.jeditable
//= require jquery.timepicker.min
//= require skrollr.min
//= require swiper.jquery
//= require swiper.jquery.min
//= require swiper.jquery.umd
//= require swiper
//= require scrollIt.js-master/scrollIt
//= require swiper.min
//= require jquery.validate