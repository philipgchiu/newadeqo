//
//= require jquery
//= require rails-ujs
//= require jquery-ui.min
//= require bootstrap-sprockets
//= require colResizable-1.6
//= require home
//= require jquery.fileDownload
//= require jquery.jeditable
//= require jquery.ellipsis
//= require bootstrap-datepicker.min
//= require Chart.min.js
//= require jquery.dataTables.min
//= require chosen.jquery.min
//= require home
//= require accounting.min
//= require selectize.min
